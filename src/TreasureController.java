import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class TreasureController implements ActionListener //listener of blocks
{
	Treasure[] Jb;
	SetTrasure settrasure;
	int x,y;
	public TreasureController(Treasure[] Jb,SetTrasure settrasure)//the construction method of listener, send every block into..
	{
		// TODO Auto-generated constructor stub
		this.Jb=Jb;
		this.settrasure=settrasure;
		x=settrasure.x;
		y=settrasure.y;
	}

	@SuppressWarnings("static-access")
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(!settrasure.mf.gameover)   //if game is not over
		for(int i=0;i<x*y;i++){      //judge which block clicked
			if(e.getSource()==Jb[i])     
			{
			if(Jb[i].isboom)          //if clicked the trap
			{
				Jb[i].setName("Trap");
				new GameOver(settrasure.mf.score,settrasure,x,y); //make the window of Gameover be instantiation
				settrasure.mf.gameover=true;
	
			}
			else
			{
				Jb[i].setName("Treasures: "+Integer.toHexString(Jb[i].value)); //show the number of Treasure
				settrasure.mf.score+=Jb[i].value; //add Treasure's numbers to final score of this time
			}
			
			}
		}
	}

}
