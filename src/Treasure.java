import javax.swing.JButton;


@SuppressWarnings("serial")
public class Treasure extends JButton  
{
    boolean isboom;            //Whether it is a trap or not
    int value;
	public Treasure(boolean isboom) {
		// TODO Auto-generated constructor stub
		this.setText("Unknown");
		this.isboom=isboom;
		value=(int) (Math.random()*9)+1;
	}
	
	public void setName(String name){
		this.setText(name);
	}
	
	public void resert(String name,boolean isboom){
		this.isboom=isboom;
		this.setName(name);
		value=(int) (Math.random()*9)+1;
	}

}
